plugins {
    id("java")
}

group = "cloud.dmytrominochkin"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.apache.logging.log4j:log4j-core:2.23.1")
    implementation("org.apache.logging.log4j:log4j-slf4j2-impl:2.23.1")

    // https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-chrome-driver
    testImplementation("org.seleniumhq.selenium:selenium-chrome-driver:4.22.0")
    testImplementation("org.seleniumhq.selenium:selenium-java:4.22.0")

    testImplementation("org.testng:testng:7.10.2")
}

tasks.test {
    useTestNG {
        suites(
            "src/test/resources/testng.xml",
            "src/test/resources/google.xml"
        )
    }
}