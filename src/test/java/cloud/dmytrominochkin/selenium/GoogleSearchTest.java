package cloud.dmytrominochkin.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * This is one way of organizing a test case - as a series of steps implemented as
 * dependent {@link org.testng.annotations.Test} methods
 */
public class GoogleSearchTest extends BaseTest {

    @Test(alwaysRun = true)
    public void goToGoogle() {
        var url = properties.getProperty("url"); // better to use constants for keys, i'm lazy :)
        driver.get(url);
        var title = driver.getTitle();
        Assert.assertEquals(title, "Google");
    }

    @Test(dependsOnMethods = {"goToGoogle"}, alwaysRun = true)
    public void searchSeleniumNoMicrosoftShown() {
        var search = driver.findElement(By.name("q"));
        search.clear();
        search.sendKeys("selenium");
        var button = driver.findElement(By.name("btnK"));
        button.click();
        Assert.assertThrows(NoSuchElementException.class, () -> {
            driver.findElement(By.cssSelector("a[href='https://microsoft.com']"));
        });
    }
}
