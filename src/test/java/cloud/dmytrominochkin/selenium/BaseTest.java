package cloud.dmytrominochkin.selenium;

import cloud.dmytrominochkin.util.ThrottlingType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chromium.ChromiumNetworkConditions;
import org.openqa.selenium.devtools.v126.network.Network;
import org.openqa.selenium.devtools.v126.network.model.ConnectionType;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.io.IOException;
import java.time.Duration;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;

public abstract class BaseTest {
    protected WebDriver driver;
    protected Properties properties;

    public BaseTest() {
        properties = new Properties();
        try (var fis = getClass().getClassLoader().getResourceAsStream("config.properties")) {
            properties.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @BeforeTest
    @Parameters(value = {"browser", "throttling"})
    public void setup(String browser, String throttling) {
        if (browser.equals("chrome")) {
            var chrome = new ChromeDriver();
            var throttlingType = ThrottlingType.valueOf(throttling.toUpperCase(Locale.ROOT));
            // Both options and devtools approach work in
            // every Chromium based browser (Chrome, Edge, etc.)
            switch (throttlingType) {
                case NONE -> {
                    // no throttling :)
                }
                case OPTIONS -> {
                    // Option 1: Using network conditions class
                    // Easy to use and focuses only on network
                    setupNetworkOptions(chrome);
                }
                case DEVTOOLS -> {
                    // Option 2: Chrome(ium) DevTools
                    // Has access to entire dev tools console for more customization
                    setupDevTools(chrome);
                }
            }
            driver = chrome;
        } else if (browser.equals("firefox")) {
            // TODO: Run firefox. I don't have firefox :)
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(500));
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }

    private static void setupDevTools(ChromeDriver chrome) {
        var devTools = chrome.getDevTools();
        devTools.createSession();
        devTools.send(Network.emulateNetworkConditions(
                false,
                150,
                10 * 8,
                10 * 8,
                Optional.of(ConnectionType.WIFI),
                Optional.empty(),
                Optional.empty(),
                Optional.empty()
        ));
    }

    private static void setupNetworkOptions(ChromeDriver chrome) {
        var networkOptions = new ChromiumNetworkConditions();
        networkOptions.setOffline(false);
        networkOptions.setLatency(Duration.ofMillis(150));
        networkOptions.setDownloadThroughput(10 * 1024); // in kb/seconds
        networkOptions.setUploadThroughput(10 * 1024); // in kb/seconds
        chrome.setNetworkConditions(networkOptions);
    }
}
