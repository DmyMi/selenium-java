package cloud.dmytrominochkin.simple;

import cloud.dmytrominochkin.Calculator;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CalcTest {

    private Calculator calc;

    @BeforeTest
    public void setup() {
        calc = new Calculator();
    }

    @Test
    public void add2And2Equals4() {
        var result = calc.add(2, 2);
        Assert.assertEquals(result, 4);
    }

    @Test
    public void add2And2Equals5() {
        var result = calc.add(2, 2);
        Assert.assertEquals(result, 5);
    }
}
