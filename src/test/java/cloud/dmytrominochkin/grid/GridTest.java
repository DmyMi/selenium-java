package cloud.dmytrominochkin.grid;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * To run GRID as a single component:
 * java -jar selenium-server-<version>.jar standalone --selenium-manager true
 */
public class GridTest {
    protected WebDriver driver;

    @BeforeTest
    public void setup() throws MalformedURLException {
        var nodeUri = "http://localhost:4444/wd/hub";
        var capabilities = new DesiredCapabilities();
        capabilities.setBrowserName("chrome");
        capabilities.setPlatform(Platform.MAC);
        driver = new RemoteWebDriver(new URL(nodeUri), capabilities);
    }

    @Test(alwaysRun = true)
    public void goToGoogle() {
        var url = "https://www.google.com";
        driver.get(url);
        var title = driver.getTitle();
        Assert.assertEquals(title, "Google");
    }
}
